<?php
# Includes
	try {
		if (
			(! @include("src/functions/shared.php"))
			||
			(! @include("src/database/database.php"))
		) {
			die('conf');
//			throw new Exception("Des fichiers sensibles sont introuvables !");
		}
	} catch (\Exception $e) {
		header('Content-type: text/html; charset=utf-8');
		die("Une erreur critique est survenue : " . $e->getMessage() . " Veuillez contacter l'administrateur du site !");
	}

# Constants
	define("DB_TYPE", "mysql");
	define("DB_HOST", "localhost");
	define("DB_USER", "admin-minichat");
	define("DB_PASS", "SL*8pVnZhPjHD7t");
	define("DB_NAME", "minichat");
	setlocale(LC_TIME, 'fr_FR.utf8','fra');

# Appels de fonction
	error_reporting(E_ALL);
	ini_set("display_errors", "On");
	@ini_set("magic_quotes_runtime", 0);
	@ini_set("magic_quotes_sybase", 0);
	removeMagicQuotes(); // Recherche des 'magic quotes' dans les variables super-globales et les supprime