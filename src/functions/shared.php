<?php
	function stripSlashesDeep($value) { // Échape en profondeur les slashes
		return (is_array($value) ? array_map("stripSlashesDeep", $value) : stripslashes(strip_tags(htmlspecialchars($value))));
	}

	function removeMagicQuotes() { // Recherche des 'magic quotes' et les supprime
		$_GET = stripSlashesDeep($_GET);
		$_POST = stripSlashesDeep($_POST);
		$_COOKIE = stripSlashesDeep($_COOKIE);
	}

	function timeAgo($time) { // Retourne une date "Human-Friendly" (ex: 'Il y a 1 minute')
		$time = strtotime($time);
		$current_time = time();
		$time_elapsed = $current_time - $time;
		$seconds = $time_elapsed ;
		$minutes = round($time_elapsed / 60 );
		$hours = round($time_elapsed / 3600);
		$days = round($time_elapsed / 86400 );
		$weeks = round($time_elapsed / 604800);
		$months = round($time_elapsed / 2600640 );
		$years = round($time_elapsed / 31207680 );
		//SECONDES
		if ($seconds <= 60) {
			return "il y a quelques secondes";
		}
		//MINUTES
		else if ($minutes <=60) {
			if ($minutes==1) {
				return "il y a 1 minute";
			} else {
				return "il y a $minutes minutes";
			}
		}
		//HEURES
		else if ($hours <=24) {
			if ($hours==1) {
				return "il y a 1 heure";
			} else {
				return "il y a $hours heures";
			}
		}
		//JOURS
		else if ($days <= 7) {
			if ($days==1) {
				return "hier";
			} else {
				return "il y a $days jours";
			}
		}
		//SEMAINES
		else if ($weeks <= 4.3) {
			if ($weeks==1) {
				return "il y a 1 semaine";
			} else {
				return "il y a $weeks semaines";
			}
		}
		//MOIS
		else if ($months <=12) {
			if ($months==1) {
				return "il y a un mois";
			} else {
				return "il y a $months mois";
			}
		}
		//ANNEE
		else{
			if ($years==1) {
				return "il y a 1 an";
			} else {
				return "il y a $years ans";
			}
		}
	}