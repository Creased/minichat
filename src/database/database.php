<?php
	function dbStart() { // Créer un nouvel objet PDO contenant une connexion à la base de données
		try {
			$dbprep = new PDO(DB_TYPE . ":host=" . DB_HOST . "; dbname=" . DB_NAME . "; charset=utf8", DB_USER, DB_PASS); // Récupère les paramètres de connexion depuis le fichier config.php
			$dbprep->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //Récupère les erreurs de requêtage sous la forme d'exceptions
			return $dbprep;
		} catch (\PDOException $e) {
			throw new \PDOException("Une erreur est survenue lors de la tentative de connexion à la base de données ! Veuillez contacter l'administrateur du site !");
		}
	}

	function dbClose($dbprep) { // Vide l'objet PDO et donc la connexion à la base de données
		$dbprep = null;
		return $dbprep;
	}

	function query($dbprep, $query, $params=array()) { // Fonction qui permet de préparer la requête avant envoie effectif avec ajout de paramètres en utilisant bindparam pour éviter les injections SQL
		try {
			$stmt = $dbprep->prepare($query);
			foreach ($params as $key => &$val) {
				if (is_int($val) || ctype_digit($val)) {
					$val = intval($val);
					$type = PDO::PARAM_INT;
				} elseif ($val === NULL) {
					$type = PDO::PARAM_NULL;
				} else {
					$type = PDO::PARAM_STR;
				}
				$stmt->bindParam($key, $val, $type); // Ajoute un paramètre à la requête PDO en spécifiant son type afin d'éviter des erreurs de type de données
			}
			$stmt->execute();
			return $stmt;
		} catch (\PDOException $e) {
			throw new \PDOException("Une erreur est survenue pendant la lecture de la base de données ! Veuillez contacter l'administrateur du site !");
		}
	}