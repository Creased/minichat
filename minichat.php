<?php
	session_start(); // On démarre la session

	try { // Includes des fichiers de configuration + Appel de la fonction pour retirer les magic-quotes qui peuvent provoquer des failles de type Injection SQL ou LFI
		if (! @include("src/config/config.php") ) {
			throw new Exception("Fichier de configuration non trouvé.");
		}
	} catch (\Exception $e) {
		header('Content-type: text/html; charset=utf-8');
		die("Une erreur critique est survenue : " . $e->getMessage() . " Veuillez contacter l'administrateur du site !");
	}

	try { // Connexion à la base de données
		$db = dbStart();
	} catch (\PDOException $e) {
		header('Content-type: text/html; charset=utf-8');
		die($e->getMessage());
	}

// Données pour la limitation d'articles affichées par page
	$page = ( isset($_GET["page"]) && ($_GET["page"] > 0) && (is_int($_GET["page"]) || ctype_digit($_GET["page"])) ) ? $_GET["page"] : 1; // Ternaire pour définir la page
	$per_page = 10; // Maximum de message par page
	$pos_pages = ($page - 1) * $per_page; // Calcul de l'offset
?>
<!DOCTYPE html>
<html lang="fr" itemscope="itemscope" itemtype="http://schema.org/QAPage">
	<head>
		<!-- assign metadata -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta name="application-name" content="Mini-chat"/>
		<meta name="description" content="TP mini-chat sur OpenClassrooms." />
		<meta name="author" content="Baptiste M." />
		<meta name="keywords" lang="fr" content="chat, mini-chat, cours php, openclassrooms" />

		<title>Mini-chat - Page <?php echo $page; ?></title>

		<link rel="stylesheet" type="text/css" href="assets/css/main.css" />

		<!-- include scripts -->
		<script src="assets/js/lib/jquery.min.js"></script>

		<!--[if lt IE 9]>
		<script src="assets/js/lib/html5shiv.js"></script>
		<![endif]-->
	</head>
	<body>
		<div id="content">
			<div class="container">
				<div class="row">
					<h1 class="page-header">{ Mini-chat } <span class="small">by Creased</span></h1>
					<div class="col-l-12 col-m-12 col-s-12">
						<form action="minichat_post.php" method="POST">
							<fieldset>
								<label for="pseudo">Pseudo : </label>
								<input type="text" name="pseudo" id="pseudo"<?php echo ( (isset($_SESSION["pseudo"])) ? " value=\"" . $_SESSION["pseudo"] . "\"" : null ); ?> required="required" />
							</fieldset>
							<fieldset>
								<label for="message">Message : </label>
								<textarea name="message" id="message" rows="3" required="required"></textarea>
								<input type="submit" value="Envoyer" />
							</fieldset>
						</form>
					</div>
					<div class="col-l-12 col-m-12 col-s-12">
						<div id="chatroom">
<?php
// Données pour la pagination
	$req = "SELECT COUNT(id) AS num FROM minichat;"; // Récupère le nombre de message
	$res = query($db, $req);
	$row = $res->fetch(PDO::FETCH_OBJ);
	$page_count = ceil($row->num / $per_page); // Récupère le nombre de page en arrondissant à l'entier supérieur

//Fermeture du curseur
	$res->closeCursor();

// Messages
	$req = "SELECT id, pseudo, message, write_datetime AS date FROM minichat ORDER BY date DESC LIMIT :page, :limit;"; // Récupère les 10 derniers messages du chat par ordre décroissant (dépend de la date)
	$params = array(":page" => $pos_pages, ":limit" => $per_page);
	$res = query($db, $req, $params);

//Traitement des données
	while ($row = $res->fetch(PDO::FETCH_OBJ)) { // Récupération récursive des données en utilisant un objet
	//Affichage des données
		echo "\t\t\t\t\t\t\t<div class=\"post\" id=\"post-$row->id\">\n";
		echo "\t\t\t\t\t\t\t\t<div class=\"meta\">\n";
		echo "\t\t\t\t\t\t\t\t\t<span class=\"author\">" . $row->pseudo . "</span>\n";
		echo "\t\t\t\t\t\t\t\t\t<span class=\"date\" title=\"" . strftime("%d %B %Y, %H:%M", strtotime($row->date)) . "\">" . timeAgo($row->date) . "</span>\n";
		echo "\t\t\t\t\t\t\t\t</div>\n";
		echo "\t\t\t\t\t\t\t\t<div class=\"message\"><p>" . $row->message . "</p></div>\n";
		echo "\t\t\t\t\t\t\t</div>\n";
	}

//Fermeture du curseur
	$res->closeCursor();

//Déconnexion de la base de données
	$db = dbClose($db);

	echo "\t\t\t\t\t\t</div>\n";

	$per_page = 8; // Maximum de bouton de navigation affichées

	if ( (isset($page_count)) && ($page_count >= $page) ) {
		if ($page_count > 1) {
			echo "\t\t\t\t\t\t<div class=\"pagination\">\n";
			echo "\t\t\t\t\t\t\t<span class=\"pageofpages\">Page $page sur $page_count</span>\n";

			if ($page == 2) {
				$url = $_SERVER["PHP_SELF"];
				echo "\t\t\t\t\t\t\t<a class=\"page previous\" href=\"$url\" title=\"Page Précédente\">&lt;</a>\n";
			} elseif ($page > 2) {
				$url = $_SERVER["PHP_SELF"] . "?page=" . ($page - 1);
				echo "\t\t\t\t\t\t\t<a class=\"page previous\" href=\"$url\" title=\"Page Précédente\">&lt;</a>\n";
			}

			$start = ($page > 1) ? ceil($page_count) : 1;
			die(ceil($page_count / $per_page) * $per_page);
			$end = (($per_page + $page) <= $page_count) ? ceil(($page) / $per_page) : $page_count;

			for ($i = $start; $i <= $end; $i++) {
				if ($i == $page) {
					echo "\t\t\t\t\t\t\t<span class=\"page current\">$page</span>\n";
				} else {
					if ($i == 1) {
						$url = $_SERVER["PHP_SELF"];
					} else {
						$url = $_SERVER["PHP_SELF"] . "?page=" . $i;
					}
					echo "\t\t\t\t\t\t\t<a class=\"page\" href=\"$url\">$i</a>\n";
				}
			}
		// Page suivante
			$url = $_SERVER["PHP_SELF"] . "?page=" . $i;
			echo "\t\t\t\t\t\t\t<a class=\"page next\" href=\"$url\" title=\"Page Suivante\">&gt;</a>\n";
			echo "\t\t\t\t\t\t</div>";
		}
	} else {
		header('Location: ' . $_SERVER["PHP_SELF"] . "?page=" . $page_count);
	}
?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>