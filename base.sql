-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Sam 04 Juillet 2015 à 21:08
-- Version du serveur: 5.5.43
-- Version de PHP: 5.4.41-0+deb7u1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `minichat`
--

DROP DATABASE IF EXISTS `minichat`;
CREATE DATABASE IF NOT EXISTS `minichat` CHARACTER SET utf8 COLLATE utf8_bin;
USE `minichat`;

DROP USER 'admin-minichat'@'localhost';
CREATE USER 'admin-minichat'@'localhost' IDENTIFIED BY 'SL*8pVnZhPjHD7t';
GRANT ALL PRIVILEGES ON minichat.* TO 'admin-minichat'@'localhost';
FLUSH PRIVILEGES;

-- --------------------------------------------------------

--
-- Structure de la table `minichat`
--

CREATE TABLE IF NOT EXISTS `minichat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(255) COLLATE utf8_bin NOT NULL,
  `message` text COLLATE utf8_bin NOT NULL,
  `write_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table contenant les messages de la chatroom' AUTO_INCREMENT=2 ;

--
-- Contenu de la table `minichat`
--

INSERT INTO `minichat` (`id`, `pseudo`, `message`, `write_datetime`) VALUES
(1, 'Creased', 'Bonjour ceci est un message de test du chat :D Comme vous le voyez il est protégé contre les injections XSS &lt;script&gt;alert(&quot;xss&quot;);&lt;/script&gt; mais essayez par vous-même ! Le pseudo n''est pas stocké dans un cookie mais dans une session pour des raisons de sécurité (je sais c''est qu''un chat mais bon ...), ainsi lorsque l''envoie de votre premier message est effectif, vous pourrez avoir le champ pré-remplie. Notez qu''il aurait été possible de vérifier les champs du formulaire côté client en utilisant Javascript mais cela n''était pas demandé ^^. Merci aux correcteurs et bon courage pour la suite ! Si vous le souhaitez, contactez moi par <a href="mailto:bap.moine.86@gmail.com" title="Contactez-moi par e-mail">mail</a> ou via <a href="https://twitter.com/Creased_" title="Suivez-moi sur Twitter">Twitter</a>.', '2015-07-04 19:42:42');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
