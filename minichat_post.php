<?php
	session_start(); // On démarre la session

	try { // Includes des fichiers de configuration + Appel de la fonction pour retirer les magic-quotes qui peuvent provoquer des failles de type Injection SQL ou XSS
		if (! @include("src/config/config.php") ) {
			throw new Exception("Fichier de configuration non trouvé.");
		}
	} catch (\Exception $e) {
		header('Content-type: text/html; charset=utf-8');
		die("Une erreur critique est survenue : " . $e->getMessage() . " Veuillez contacter l'administrateur du site !");
	}

	try { // Connexion à la base de données
		$db = dbStart();
	} catch (\PDOException $e) {
		header('Content-type: text/html; charset=utf-8');
		die($e->getMessage());
	}

	if (
		($pseudo = ( isset($_POST["pseudo"]) ) ? $_POST["pseudo"] : null ) // Évalue de manière booléenne l'affectation de la variable title en utilsant un ternaire, si isset renvoie false alors title prend la valeur null, l'affectation correspond donc à un booléen false, sinon title prend la valeur de title dans $_POST et renvoie donc un booléen true
		&&
		($message = ( isset($_POST["message"]) ) ? $_POST["message"] : null )
	) {
		if ($pseudo <> $_SESSION["pseudo"]) { // On modifie le pseudo dans la variable de session s'il y'a modification
			$_SESSION["pseudo"] = $pseudo;
		}

		//Envoi d'une requête
		$req = "INSERT INTO `minichat` (
					`pseudo`,
					`message`
				) VALUES (
					:pseudo,
					:message
				);";
		$params = array(":pseudo" => $pseudo, ":message" => $message);
		query($db, $req, $params); // Afin d'éviter les injections SQL on utilise les PDO params

		// Redirection vers la page du minichat
		header('Location: minichat.php');
	} else {
		header("500 Internal Server Error; Content-type: text/html; charset=utf-8", true, 500);
		die("Erreur: Veuillez remplir le formulaire au complet !");
	}